## Checkpoint Client - Linux Version

#### Versão do Client Checkpoint para Linux utilizando o pacote SNX.

### Instalação

- Clone ou faça download deste projeto
- Dê permissão de execução e em seguida execute o script de instalação de acordo com a Distro Linux em uso:

Para Distros que utilizam pacotes .deb (Debian, Ubuntu, etc):
```
chmod +x deb_install.sh
sh deb_install.sh 
```

Para Distros que utilizam pacotes .rpm (Fedora, etc):
```
chmod +x rpm_install.sh
sh rpm_install.sh 
```

### Conectando a VPN
```
snx -s <<ip-de-acesso-da-vpn> -u <nome-do-usuario-da-vpn>
```

### Desconectando a VPN
```
snx -d
```

Para utilização de software de RDP (Remote Desktop Protocol), utilizo o pacote **Remmina**, que me atende muito bem e indico sua utilização. 
