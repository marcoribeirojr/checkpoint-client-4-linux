#!/bin/bash

sudo dnf install -y nss-tools openssl xterm
sudo dnf install -y glibc.i686 pam.i686 libX11.i686 libnsl.i686
wget https://www.rpmfind.net/linux/centos/7.9.2009/os/x86_64/Packages/compat-libstdc++-33-3.2.3-72.el7.i686.rpm
sudo rpm -i --nosignature compat-libstdc++-33-3.2.3-72.el7.i686.rpm
rm compat-libstdc++-33-3.2.3-72.el7.i686.rpm

# Instalação da aplicação snx, que faz a conexão com a VPN para Linux
chmod +x snx_install_linux30.sh
sudo sh snx_install_linux30.sh

