#!/bin/bash

sudo dpkg --add-architecture i386
sudo apt install -y libnss3-tools openssl xterm
sudo apt install -y libpam0g:i386 libx11-6:i386 libstdc++6:i386 libstdc++5:i386

# Instalação da aplicação snx, que faz a conexão com a VPN para Linux
chmod +x snx_install_linux30.sh
sudo sh snx_install_linux30.sh


